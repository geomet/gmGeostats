## Test environments
  
* Linux: Ubuntu 18.04.4LTS  (local)
  - R 3.6.3 (production)
  - R 4.2.3 patched (2023-03-29 r84146) (test)
  - R devel (2023-04-02 r84146) (test)
* Windows 7 Enterprise
  - R version 4.2.2 (2022-10-31 ucrt) (test)

  
## R CMD check results
  
  0 ERRORS
  0 WARNINGS
  1 NOTES (new resubmission after archiving)
  
  
## This is a resubmission, after archiving on 2023-04-05


### Comments by Victoria Wimmer on 2023-04-14

The following issues have been corrected or commented here inline:

```
Thanks,

Please proof-read your description text.
Currently it reads: "... positive amounts data ..."
Probably it should rather be: "... positive amounts of data ..."
```

ANSWER: Description proofread and slightly changed. The specific sentence was correct as it was: it is about data constituted by positive numbers, i.e. amounts. Hopefully the new description is clearer.

```
Please add \value to .Rd files regarding exported methods and explain the functions results in the documentation. Please write about the structure of the output (class) and also what the output means. (If a function does not return a value, please document that too, e.g. \value{No return value, called for side effects} or similar)
Missing Rd-tags:
        xvErrorMeasures.default.Rd: \value
```        
   
ANSWER: Done. Also a section Details has been added.        

```
\dontrun{} should only be used if the example really cannot be executed (e.g. because of missing additional software, missing API keys, ...) by the user. That's why wrapping examples in \dontrun{} adds the comment ("# Not run:") as a warning for the user.
Does not seem necessary.
Please unwrap the examples if they are executable in < 5 sec, or replace \dontrun{} with \donttest{}.
```

ANSWER: The following instances of \dontrun{} were found. All of them had to be preserved for the specific reasons mentioned below. Please advice specifically to other strategies if this is not acceptable.

noSpatCorr.test() is a very slow function, even with the small reproducible example given in the examples section;. 

getTellus() downloads a data set from an internet location;  \dontrun{} is necessary as to avoid writing in the user's filespace.

validate.test() is also a very slow function. 

sortDataInGrid() and image_cokriged() examples both depend on getTellus() in order to run, and running needs to be avoided for the same reason. 

write.GSLib() also generates a foreign file in the user's file space, giving problems if run (as getTellus() above).

gsi.DS() is also a very slow function. 

The overarching problem is that, dealing with spatial data, examples quickly become quite complex (and computations several minutes long) if they have to be relevant and intuitive for users to understand. 


```
You write information messages to the console that cannot be easily suppressed. It is more R like to generate objects that can be used to extract the information a user is interested in, and then print() that object.
Instead of print()/cat() rather use message()/warning()  or if(verbose)cat(..) (or maybe stop()) if you really have to write text to the console.
(except for print, summary, interactive functions)
```

ANSWER: No print() usage was found outside print() or summary() methods.
Some cat() were found in print() or summary() methods, and left as they were. All other cat() messages were converted to message() as requested. 

```
Please ensure that your functions do not write by default or in your examples/vignettes/tests in the user's home filespace (including the package directory and getwd()). This is not allowed by CRAN policies.
Please omit any default path in writing functions. In your examples/vignettes/tests you can write to tempdir().
```

ANSWER: We only found two functions writing to the user homespace, write.GSLib() and getTellus(), and both are doing that at the request of the user; write.GSLib() is just a write.*() function, and getTellus() downloads an illustration data set from a server of the Geological Survey of Northern Ireland. This solution had to be adopted for licensing reasons, as the GSNI allows to use the data but not to redistribute it. We are not sure which other function/vignette might be meant here.  

```
Please make sure that you do not change the user's options, par or working directory. If you really have to do so within functions, please ensure with an *immediate* call of on.exit() that the settings are reset when the function is exited. e.g.:
...
oldpar <- par(no.readonly = TRUE)    # code line i
on.exit(par(oldpar))            # code line i + 1
...
par(mfrow=c(2,2))            # somewhere after
...

...
old <- options()         # code line i
on.exit(options(old))      # code line i+1
...
options(digits = 3)
...

...
oldwd <- getwd()           # code line i
on.exit(setwd(oldwd))        # code line i+1
...
setwd(...)            # somewhere after
...
If you're not familiar with the function, please check ?on.exit. This function makes it possible to restore options before exiting a function even if the function breaks. Therefore it needs to be called immediately after the option change within a function.

Please always make sure to reset to user's options(), working directory or par() after you changed it in examples and vignettes and demos. -->  man/constructMask.Rd; man/sphTrans.Rd 
e.g.:
oldpar <- par(mfrow = c(1,2))
...
par(oldpar)

Please fix and resubmit.

Best,
Victoria Wimmer
```

ANSWER: The following instances of par() or options() were found

pwlrmap(), pairsmap.default(), plot.gmCgram(), plot.gmEVario(), variogramModelPlot.gstatVariogram(), plot.logratioVariogramAnisotropy() : these functions implement the `on.exit(par(opar))` solution, albeit conditionally on the user not wanting the device to remain open. This is achieved by a logical flag `closeplot` which default value is TRUE, hence giving the requested behavour. We have added the extra argument `no.readonly = TRUE` to `opar = par()` so that now `oldpar<-par(...)` and `on.exit(...)` are consecutive as requested.  

swath.default(), swath.acomp(), image.logratioVariogramAnisotropy(), plot.accuracy():  We have added the extra argument `no.readonly = TRUE` to `opar = par()` so that now `oldpar<-par(...)` and `on.exit(...)` are consecutive as requested.  

About options(): only a package-wide edit of options occurs. This is introduced in .onLoad() and removed on .onUnload(). Please advice if this is not the correct way to do it.

About working directory manipulations: this only occurs at the request of the user by calling function getTellusData() (see above as well). This function now implements the proposed solution of
```
  wd0 = getwd()
  on.exit(setwd(wd0))
  setwd(wd) # wd is an argument to getTellusData() provided by the user.
```

The occurrences in man/constructMask.Rd and man/sphTrans.Rd were also corrected. 

```
Additionally:
Have the issues why your package was archived been fixed?
Please explain this in the submission comments.
```

ANSWER: Yes, these are reported in the next section, "Original request".

best regards

### Original request


ANSWER: Request from 2023-03-19. All calls to .C() are now explicitly pointing to symbols instead of function namestrings. Package "gmGeostats" could  not be uploaded before the archiving deadline because the main dependence "compositions" was showing the same problems as well, and it took some time  to correct it under macos ARM flavor. 

```
Dear maintainer,

Please see the problems shown on
<https://cran.r-project.org/web/checks/check_results_gmGeostats.html>.

The "DLL requires the use of native symbols" errors in the
r-devel-linux-x86_64-debian-gcc check results are from

r83985 | luke | 2023-03-16 02:31:57 +0100 (Thu, 16 Mar 2023) | 3 lines
Enforce R_forceSymbols for namespaces, but for now only if an
environment variable is set.

which makes R_forceSymbols work correctly, for now provided that env var
R_REALLY_FORCE_SYMBOLS is set.

You can conveniently extract the non-symbols in your package's foreign
function calls via tools:::package_native_routine_registration_db(),
e.g.,

R> tools:::package_native_routine_registration_db("/data/rsync/PKGS/tibble")
  cname                  s n
1 .Call tibble_need_coerce 1

but please note that there may be several such calls in your code.

Please correct before 2023-04-02 to safely retain your package on CRAN.

Best,
-k
```