Package: gmGeostats
Version: 0.11.3
Date: 2023-04-17
Title: Geostatistics for Compositional Analysis
Authors@R: c(person(given = "Raimon", 
        family = "Tolosana-Delgado", 
        role = c("aut"),
        email = "r.tolosana@hzdr.de",
        comment = c(ORCID = "0000-0001-9847-0462")),
        person(given = "Ute", 
        family = "Mueller", 
        role = c("aut"),
        email = "u.mueller@ecu.edu.au"),
        person(given = "K. Gerald", 
        family = "van den Boogaart", 
        role = c("ctb", "cre"),
        email = "support@boogaart.de"),
        person(given = "Hassan", 
        family = "Talebi", 
        role = c("ctb", "cph"),
        email = "hassan.talebi@csiro.au"),
        person(given = "Helmholtz-Zentrum Dresden-Rossendorf", role = "cph"),
        person(given = "Edith Cowan University", role = "cph")
     )
Depends: 
    R (>= 2.10)
Suggests: 
    FNN,
    JADE,
    jointDiag,
    DescTools,
    knitr,
    rmarkdown (>= 2.3),
    magrittr,
    readxl
Imports: 
    methods, 
    gstat, 
    compositions (>= 2.0), 
    sp, 
    boot, 
    foreach, 
    utils, 
    RColorBrewer
Description: Support for geostatistical analysis of multivariate data, 
         in particular data with restrictions, e.g. positive amounts, 
         compositions, distributional data, microstructural data, etc. 
         It includes descriptive analysis and modelling for such data, both 
         from a two-point Gaussian perspective and multipoint perspective.
         The methods mainly follow Tolosana-Delgado, Mueller and van den
         Boogaart (2018) <doi:10.1007/s11004-018-9769-3>.
License: CC BY-SA 4.0 | GPL (>=2)
URL: https://codebase.helmholtz.cloud/geomet/gmGeostats
Copyright: (C) 2020 by Helmholtz-Zentrum Dresden-Rossendorf and Edith Cowan University;
         gsi.DS code by Hassan Talebi
RoxygenNote: 7.1.1
Roxygen: list(markdown = TRUE)
Encoding: UTF-8
VignetteBuilder: knitr
LazyData: true
Collate: 
    'Anamorphosis.R'
    'preparations.R'
    'gstatCompatibility.R'
    'gmAnisotropy.R'
    'compositionsCompatibility.R'
    'variograms.R'
    'gmSpatialMethodParameters.R'
    'abstractClasses.R'
    'accuracy.R'
    'closeup.R'
    'data.R'
    'exploratools.R'
    'genDiag.R'
    'geostats.R'
    'gmDataFrameStack.R'
    'gmSimulation.R'
    'gmSpatialModel.R'
    'gmValidationStrategy.R'
    'grids.R'
    'mask.R'
    'spSupport.R'
    'uncorrelationTest.R'
    'zzz.R'
